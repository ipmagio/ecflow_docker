# ecFlow server

This image contains ECMWF's [ecFlow](https://software.ecmwf.int/wiki/display/ECFLOW/Home).

It is meant to be used as one ecFlow server per container, 
even if ecFlow allows spwaning multiple servers on a single host.

This build of ecFlow has Python support.

Relevant docker details:

* The server is listening on tcp port 3141;
* Logs are being redirected from ecFlow's own log file to stdout
so that Docker can handle them;
* The image creates a volume at /opt/ecflow

Run the image with:

    docker run --detach --name my-ecflow -p 3141:3141

Now inspect the logs in order to confirm that ecFlow is running:

    docker logs my-ecflow

## Communicating with a running container

    docker run -ti -link my-ecflow:ecflow ecflow-server
    # ecflow_client --ping --host=ecflow

This image does not feature any ecflow gui
