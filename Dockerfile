FROM ubuntu:16.04

MAINTAINER Ricardo Garcia Silva

# * could use qtbase5-dev if we wanted ecflow's new Qt5 UI
RUN apt-get update && apt-get install -y \
    build-essential \
    cmake \
    python-dev \
    wget

# Environment flags used for compilation (they can be removed after we build)
ENV \
    ECFLOW_VERSION=4.1.0 \
    BOOST_VERSION=1_53_0 \
    WK=/tmp/ecflow_build/ecFlow-4.1.0-Source \
    BOOST_ROOT=/tmp/ecflow_build/boost_1_53_0

WORKDIR /tmp

# downloading, compiling and deleting in one single docker layer, to keep the image size low
RUN \
    mkdir ecflow_build && \
    cd ecflow_build && \
    wget --output-document=ecflow-${ECFLOW_VERSION}.tar.gz https://software.ecmwf.int/wiki/download/attachments/8650755/ecFlow-${ECFLOW_VERSION}-Source.tar.gz?api=v2 && \
    wget --output-document=boost_${BOOST_VERSION}.tar.gz https://software.ecmwf.int/wiki/download/attachments/8650755/boost_${BOOST_VERSION}.tar.gz?api=v2 && \
    tar -zxvf ecflow-${ECFLOW_VERSION}.tar.gz && \
    tar -zxvf boost_${BOOST_VERSION}.tar.gz && \
    cd ${BOOST_ROOT} && \
    ./bootstrap.sh && \
    ${WK}/build_scripts/boost_1_53_fix.sh && \
    ${WK}/build_scripts/boost_build.sh && \
    cd ${WK} && \
    mkdir build && \
    cd build && \
    cmake .. \
        -DENABLE_GUI=OFF \
        -DENABLE_UI=OFF && \ 
    make -j2 && \
    make install && \
    make test  && \
    cd /tmp && \
    rm -rf * && \
    apt-get remove -y \
        build-essential \
        cmake \
        python-dev \
        wget

# ecflow places the python bindings in site-packages while debian based distros
# use dist-packages instead, this makes the bindings known to the system
ENV PYTHONPATH=/usr/local/lib/python2.7/site-packages

# environment variables used by ecflow_server
ENV \
    ECFLOW_USER=ecflow \
    ECF_PORT=3141 \
    ECF_HOME=/opt/ecflow \
    ECF_LOG=/opt/ecflow/ecflow.log

WORKDIR ${ECF_HOME}

# Redirecting ecflow server's log files to stdout so that Docker can manage them
RUN ln -sf /dev/stdout ${ECF_LOG}

# expose ecf_port
EXPOSE ${ECF_PORT}

VOLUME ${ECF_HOME}

# switch to unprivileged user
RUN groupadd --system ${ECFLOW_USER} && useradd --create-home --system --gid ${ECFLOW_USER} ${ECFLOW_USER}
USER ${ECFLOW_USER}

# Start the ecflow server
CMD ["ecflow_server"]
